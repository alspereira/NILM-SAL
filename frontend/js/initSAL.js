var $ = jQuery.noConflict();
$(window).on('load', loadInitialData)
$.ajaxSetup({ cache: false });
// $(window).load(loadInitialData)
//$(document).ready(loadInitialData)

datasetInfo = {}
testInfo = {}
plugInfo = {}

table_sufix = ""
plug_address = ""
test_id = ""

function loadInitialData(){
    //http://aveiro.m-iti.org/hybridnilm-sal/sal.html?test=1&plug=000D6F000261B01C&startDate=2017-01-03&endDate=2017-01-04
    //console.log('window load')

    //http://aveiro.m-iti.org:8000/api/sample/plug_add/000D6F000099265F/dateStart/2016-12-01/dateEnd/2016-12-01
    //http://aveiro.m-iti.org:8000/api/get-plugs
    //http://aveiro.m-iti.org:8000/api/get-tests

    getDatasets(function(data) {
        // get the dataset in the initial position
        table_sufix = datasetInfo[0]['sufix']
        getPlugs(function(data) {
            plug_address = plugInfo[0]['id']
            console.log(plug_address)
            getTests(function(data) {
                console.log(data)
                fillControls(true)
                updateUrl()
                addListenersToSelect()
            })
        })

       /* async.parallel(
        [
            getPlugs,
            getTests,
        ], function(err, res){
            console.log('fim do parallel')
            fillControls()
        })*/
    })
    
    /*getDatasets(function(data) {
        // get the dataset in the initial position
        table_sufix = datasetInfo[0]['sufix']
        
        async.parallel(
        [
            getPlugs,
            getTests,
        ], function(err, res){
            console.log('fim do parallel')
            fillControls()
        })
    })*/

    $('.date').datepicker({dateFormat: 'yy-mm-dd'});
    
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    })
    
    var dateStartString = Date.today().addDays(-1).toString('yyyy-MM-dd')
    var dateEndString = Date.today().toString('yyyy-MM-dd')
    $('#selectStartDate').val(dateEndString)
    $('#selectEndDate').val(dateEndString)


    $('#shiftDaysLeft').click(function(){
        var sysState = getUIValues()
        sysState.startDateString = new Date(sysState.startDateString).addDays(-1).toString('yyyy-MM-dd')
        sysState.endDateString = new Date(sysState.endDateString).addDays(-1).toString('yyyy-MM-dd')
        $('#selectStartDate').val(sysState.startDateString)
            $('#selectEndDate').val(sysState.endDateString)
        updateUrl()
        addDataToChart(sysState.selectedDataset, sysState.selectedTest, sysState.selectedPlug, sysState.startDateString, sysState.startDateString, function(){
            shiftChart(sysState.startDateString, sysState.endDateString)
        })
    })

    $('#shiftDaysRight').click(function(){
        var sysState = getUIValues()
        sysState.startDateString = new Date(sysState.startDateString).addDays(1).toString('yyyy-MM-dd')
        sysState.endDateString = new Date(sysState.endDateString).addDays(1).toString('yyyy-MM-dd')
            $('#selectStartDate').val(sysState.startDateString)
        $('#selectEndDate').val(sysState.endDateString)
        updateUrl()
        addDataToChart(sysState.selectedDataset, sysState.selectedTest, sysState.selectedPlug, sysState.endDateString, sysState.endDateString, function(){
            shiftChart(sysState.startDateString, sysState.endDateString)
        })
    })
}

function preSelectFromQueryParams(){
    var dsId = getUrlParameterByName('ds')
    var testId = getUrlParameterByName('test')
    var PlugId = getUrlParameterByName('plug')
    var startDate = getUrlParameterByName('startDate')
    var endDate = getUrlParameterByName('endDate')

    if(dsId != undefined){
       $('#selectDataset').val(dsId)
    }
    if(testId != undefined){
        $('#selectTest').val(testId)
    }
    if(PlugId != undefined){
        $('#selectPlug').val(PlugId)
    }
    if(startDate != undefined){
        $('#selectStartDate').val(startDate)
    }
    if(endDate != undefined){
        $('#selectEndDate').val(endDate)
    }
}

// function to get datasets
function getDatasets(cb) {
    $.ajax({url:'http://aveiro.m-iti.org:8000/api/get-datasets', cache: false, success:function(data){
            console.log('got datasets')
            datasetInfo = data
            if (typeof(cb) == 'function') { cb() }
        }}).done(function(data){
            console.log('DONE')
        }).fail(function(data){
            console.log('fail')
        })
}

function getPlugs(cb){
    console.log("dsada: " + table_sufix)
    $.ajax({url:'http://aveiro.m-iti.org:8000/api/get-plugs/' + table_sufix, cache: false, success:function(data){
        console.log('got plugs')
        plugInfo = data
        if (typeof(cb) == 'function') { cb() }
    }}).done(function(data){
        console.log('DONE')
    }).fail(function(data){
        console.log('fail')
    })
}
/*
function getPlugs(cb){
    $.ajax({url:'http://aveiro.m-iti.org:8000/api/get-plugs', cache: false, success:function(data){
        console.log('got plugs')
        plugInfo = data
        if (typeof(cb) == 'function') { cb() }
    }}).done(function(data){
        console.log('DONE')
    }).fail(function(data){
        console.log('fail')
    })
}*/

/*function getTests(cb){
    $.get('http://aveiro.m-iti.org:8000/api/get-tests/ds/' + table_sufix + '/plug_add/' +  plug_address + '?c=' + new Date().getTime(), function(data){
        console.log('got tests')
        testInfo = data
        if (typeof(cb) == 'function') { cb() }
    }) 
}*/

function getTests(cb){
    $.get('http://aveiro.m-iti.org:8000/api/get-tests/ds/' + table_sufix + '?c=' + new Date().getTime(), function(data){
        console.log('got tests')
        testInfo = data
        if (typeof(cb) == 'function') { cb() }
    }) 
}

/*function getTests(cb){
    $.get('http://aveiro.m-iti.org:8000/api/get-tests' + '?c=' + new Date().getTime(), function(data){
        console.log('got tests')
        testInfo = data
        if (typeof(cb) == 'function') { cb() }
    }) 
}*/

function fillControls(dataset){
    if(dataset == true)
        addToSelect('selectDataset', datasetInfo, 'sufix', 'dataset')
    addToSelect('selectPlug', plugInfo, 'id', 'appliance_name')
    addToSelect('selectTest', testInfo, 'test_id', 'test_id')
    preSelectFromQueryParams()
    //stylifySelect()
}

function addListenersToSelect(){
    $("#selectDataset").change(selectionChangeDataset)
    $("#selectTest").change(selectionChange)
    $("#selectPlug").change(selectionChange)
    $("#selectStartDate").change(selectionChange)
    $("#selectEndDate").change(selectionChange)
    selectionChange()
}

function selectionChangeDataset() {
    console.log("dataset change")
    var selectedDatasetOption = $("#selectDataset option:selected")[0];
    table_sufix = selectedDatasetOption.value
    updateUrl()
    console.log(table_sufix)
    getPlugs(function(data) {
        plug_address = plugInfo[0]['id']
        console.log(plug_address)
        getTests(function(data) {
            console.log(data)
            fillControls(false)
        })
    })
}


function selectionChangePlug() {
    var selectedPlugOption = $("#selectPlug option:selected")[0];
    var selectedPlug = selectedPlugOption.value;
    // TODO: load the tests for this plug
}

function selectionChange(){
    var sysState = getUIValues()
    updateUrl()
    prepareBarChart(sysState.selectedDataset, sysState.selectedTest, sysState.selectedPlug, sysState.startDateString, sysState.endDateString)
}

function updateUrl(){
    var sysState = getUIValues()
    
    window.history.pushState('HybridNILM - SAL', 'HybridNILM - SAL', 'sal.html?ds=' + sysState.selectedDataset + '&test=' + sysState.selectedTest + '&plug='+ sysState.selectedPlug +'&startDate=' + sysState.startDateString + '&endDate=' + sysState.endDateString);
}

function getUIValues() {
    var selectedDatasetOption = $("#selectDataset option:selected")[0];
    var selectedPlugOption = $("#selectPlug option:selected")[0];
    var selectedTestOption = $("#selectTest option:selected")[0];
    
    if(selectedDatasetOption == undefined) return   // not possible to continute without a dataset
    if(selectedPlugOption == undefined) return      // not possible continute without a plug
    
    //var testId = selectedDatasetOption.value
    var selectedDatasetName = selectedDatasetOption.innerHTML
    var selectedDataset = selectedDatasetOption.value
    var selectedPlug = selectedPlugOption.value
    var selectedTest = 0

    if(selectedTestOption != undefined)
        selectedTest = selectedTestOption.value

    console.log(selectedDataset)
    console.log(selectedPlug)
    console.log(selectedTest)

    var startDateString = $("#selectStartDate").val()
    var endDateString = $("#selectEndDate").val()

    return {
        selectedDatasetName: selectedDatasetName,
        selectedDataset: selectedDataset,
        selectedPlug: selectedPlug,
        selectedTest: selectedTest,
        startDateString: startDateString,
        endDateString: endDateString
    }
}
