var weekday = new Array(7);
  weekday[0]=  "Sunday";
  weekday[1] = "Monday";
  weekday[2] = "Tuesday";
  weekday[3] = "Wednesday";
  weekday[4] = "Thursday";
  weekday[5] = "Friday";
  weekday[6] = "Saturday";

function getUrlSegments(){
    pathArray = window.location.pathname.split( '/' );
    pathArray.splice(0,1)
    return pathArray
}

function getUrlParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getDateStrFromEpoch(epoc){
    var date = new Date(epoc)
    var month = date.getMonth() < 9 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1)
    var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    return date.getFullYear() + '-' + month + '-' + day;
}

function getHourStrFromEpoch(epoc, seconds){
    var date = new Date(epoc)
    var hour = date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
    var minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
    var seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
    if(seconds){
      return hour + ':' + minutes + ':' + seconds
    }
    return hour + ':' + minutes;
}

function dateToYMDFormat(date, includeTime){
    if(date == undefined)
      date = new Date()

    var dateString = date.getFullYear()
    dateString += '-'
    dateString += date.getMonth() < 9 ?  "0" + (date.getMonth() + 1) : (date.getMonth() + 1)
    dateString += '-'
    dateString += date.getDate() < 10 ?  "0" + date.getDate() : date.getDate()
    if(includeTime){
      dateString += ' '
      dateString += date.getHours() < 10 ?  "0" + date.getHours() : date.getHours()
      dateString += ":"
      dateString += date.getMinutes() < 10 ?  "0" + date.getMinutes() : date.getMinutes()
    }
    return dateString;
}

function range(start, stop, step) {
    if (typeof stop == 'undefined') {
        // one param defined
        stop = start;
        start = 0;
    }

    if (typeof step == 'undefined') {
        step = 1;
    }

    if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
        return [];
    }

    var result = [];
    for (var i = start; step > 0 ? i < stop : i > stop; i += step) {
        result.push(i);
    }

    return result;
};

function dateSumDays(date, days){
  return new Date(new Date(date).setDate(date.getDate() + days))
}

jQuery.fn.toggleSwitch = function(params) {

    var defaults = {
        highlight: true,
        width: 25,
        change: null
    };

    var options = $.extend({}, defaults, params);

    $(this).each(function(i, item) {
        generateToggle(item);
    });

    function generateToggle(selectObj) {

        // create containing element
        var $contain = $("<div />").addClass("ui-toggle-switch");

        // generate labels
        $(selectObj).find("option").each(function(i, item) {
            $contain.append("<label>" + $(item).text() + "</label>");
        }).end().addClass("ui-toggle-switch");

        // generate slider with established options
        var $slider = $("<div />").slider({
            min: 0,
            max: 100,
            animate: "fast",
            change: options.change,
            stop: function(e, ui) {
                var roundedVal = Math.round(ui.value / 100);
                var self = this;
                window.setTimeout(function() {
                    toggleValue(self.parentNode, roundedVal);
                }, 11);
            },
            range: (options.highlight && !$(selectObj).data("hideHighlight")) ? "max" : null
        }).width(options.width);

        // put slider in the middle
        $slider.insertAfter(
        $contain.children().eq(0));

        // bind interaction
        $contain.delegate("label", "click", function() {
            if ($(this).hasClass("ui-state-active")) {
                return;
            }
            var labelIndex = ($(this).is(":first-child")) ? 0 : 1;
            toggleValue(this.parentNode, labelIndex);
        });

        function toggleValue(slideContain, index) {
            $(slideContain).find("label").eq(index).addClass("ui-state-active").siblings("label").removeClass("ui-state-active");
            $(slideContain).parent().find("option").eq(index).attr("selected", true);
            $(slideContain).find(".ui-slider").slider("value", index * 100);
        }

        // initialise selected option
        $contain.find("label").eq(selectObj.selectedIndex).click();

        // add to DOM
        $(selectObj).parent().append($contain);

    }
};

function addToSelect(select, arr){
  addToSelect(select, arr, 'id', 'name')
}

function addToSelect(select, arr, id, name){
document.getElementById(select).options.length = 0
  arr.forEach(function(item){
      var opt = document.createElement("option");
      opt.value = item[id]
      opt.text  = item[name]
      document.getElementById(select).appendChild(opt)
  })
}

// function addPagesToSelect(pages){
//   var segments = getUrlSegments()
//   pages.forEach(function(page){
//       var opt = document.createElement("option");

//       if(page.url == segments[0]){
//           opt.selected = "true"  
//       }

//       opt.value = page.url
//       opt.text  = page.name

//       document.getElementById('selectPage').appendChild(opt)
//   }) 
// }

// function addInfoToLegend(elements){
// 	var elementArray = elements.map(function(divName){
// 		return document.getElementById(divName)
//     })

//     routersData.forEach(function(router){
// 	    elementArray.forEach(function(element){
// 	    	var li = document.createElement("li");
// 	    	li.innerHTML = '<span class="ulMarker" style="color:' + router.color + '">● &nbsp;</span>' + router.name
// 	    	element.appendChild(li)
// 	    })
//     })
// }

// function getRouterById(id){
//     return routersData.filter(function(el){
//         return el.id == id
//     })[0]
// }

// function setRouterEnabled(id, enabled){
//   routersData.forEach(function(el, index){
//     if(el.id == id)
//       routersData[index].enabled = enabled
//   })
// }

function stylifySelect(){
  // $('select').change(function() {
  //   var selectedOptionElement = $(this).children("option:selected")[0];
  //   console.log(selectedOptionElement.value)
  // })

  $('select').each(function() {
    var $this = $(this),
      numberOfOptions = $(this).children('option').length;

    $this.addClass('select-hidden');
    $this.wrap('<div class="select ' + $this.attr('id') + '"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.addClass($this.attr('id'))
    if($this.children('option:selected')[0] != undefined){
      $styledSelect.text($this.children('option:selected').eq(0).text())
    }else{
      $styledSelect.text($this.children('option').eq(0).text());
    }



    var $list = $('<ul />', {
      'class': 'select-options'
    }).insertAfter($styledSelect);
    // $list.addClass($this.attr('id'))
    
    for (var i = 0; i < numberOfOptions; i++) {
      $('<li />', {
        text: $this.children('option').eq(i).text(),
        rel: $this.children('option').eq(i).val()
      }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function(e) {
      e.stopPropagation();
      $('div.select-styled.active').each(function() {
        $(this).removeClass('active').next('ul.select-options').hide();
      });
      $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function(e) {
      e.stopPropagation();
      $styledSelect.text($(this).text()).removeClass('active');
      $this.val($(this).attr('rel'));
      $list.hide();

      //console.log($this.val());
      // MIGUEL CUSTOM CODE
      // #############################################################################
      // #############################################################################
      var option = $this.children("[value='" + $this.val() + "']" )
      option.prop('selected', true);
      $this.change(); 
      // #############################################################################
      // #############################################################################
    });

    $(document).click(function() {
      $styledSelect.removeClass('active');
      $list.hide();
    });

  });
}