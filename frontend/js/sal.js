var chartObj;

// $("#btn_cancel").click(function(){
//     $("#vote_transition").css("display", "none")
// })

colors= {
    RED: '#b7180a',
    GREY : '#9e9e9e',
    GRAY : '#b7180a',
    GREEN: 'green'
}
var seriePower = []
var serieState = []
var serieStateHuman = []

var table_sufix;
var testId;
var macAdd;
var startDateStr;
var endDateStr;

var maxPowerValue = 0

function prepareBarChart(aTable_sufix, aTestId, aMacAdd, aStartDateStr, aEndDateStr)
{
    table_sufix = aTable_sufix
    testId = aTestId
    macAdd = aMacAdd
    startDateStr = aStartDateStr
    endDateStr = aEndDateStr

    if(chartObj != undefined)
        chartObj.showLoading()

    async.parallel([
        getEvent,
        getEventHuman,
        getSample,
    ],function(err, res){
        if(err){
             window.alert('An error occured getting the data')
        }
        preDrawChart()
    })
    function preDrawChart(){
        maxPowerValue = Math.max(maxPowerValue, 50)
        if(chartObj != undefined)
            chartObj.hideLoading()
        drawChart(seriePower, serieState, serieStateHuman, maxPowerValue)
    }
}

function getSample(cb){
    seriePower = []
    var url = 'http://aveiro.m-iti.org:8000/api/sample_ssd/ds/' + table_sufix + '/plug_add/' + macAdd + '/startDate/' + startDateStr + '/endDate/' + endDateStr
    //timestamp,power
    $.get(url,function(data){
        data.forEach(function(el){
            var power = el[1]
            if(power > maxPowerValue){
                maxPowerValue = power
            }
            var date = Date.parse(el[0]).getTime()
            seriePower.push([date, power])
        })
        if (typeof(cb) == 'function') cb()
    })
}

function getEvent(cb){
    serieState = []
    var url = 'http://aveiro.m-iti.org:8000/api/event/ds/' + table_sufix + '/test/' + testId + '/plug_add/' + macAdd + '/startDate/' + startDateStr + '/endDate/' + endDateStr
    //id,test_id,timestamp,initial_power,delta,active
    $.get(url,function(data){
        data.forEach(function(el){
            var date = Date.parse(el[2]).getTime()
            var pointObj = {x: date, y: el[3] + Math.abs(el[4]), id:el[0], active:el[5]}
            pointObj.color = pointObj.active == 1 ? colors.RED : colors.GREY
            pointObj.fill = pointObj.color
            pointObj.marker = { fillColor: pointObj.color }
            serieState.push(pointObj)
        })
        if (typeof(cb) == 'function') cb()
    })
}

function getEventHuman(cb){
    serieStateHuman = []
    var url = 'http://aveiro.m-iti.org:8000/api/event-human/ds/' + table_sufix + '/test/' + testId + '/plug_add/' + macAdd + '/startDate/' + startDateStr + '/endDate/' + endDateStr
    //id,test_id,timestamp,initial_power,delta,active,nr_add,nr_rem
    $.get(url,function(data){
        data.forEach(function(el){
            if(el[5] != 0){
                var date = new Date(el[2]).getTime()
                serieStateHuman.push({x:date, y: el[3] + Math.abs(el[4]), id:el[0], active: el[5]})
            }
        })
        if (typeof(cb) == 'function') cb()
    })
}

function addDataToChart(table_sufix, testId, macAdd, aStartDateStr, aEndDateStr, cb){
    //adjust global vars that will be accessed by the requesting functions
    startDateStr = aStartDateStr
    endDateStr = aEndDateStr

    serieState = []
    serieStateHuman = []
    seriePower = []
    if(chartObj != undefined)
        chartObj.showLoading()
    async.parallel([
        getEvent,
        getEventHuman,
        getSample,
    ],function(err, res){
        if(err){
             window.alert('An error occured getting the data')
        }
        maxPowerValue = Math.max(maxPowerValue, 50)
        if(chartObj != undefined)
            chartObj.hideLoading()
        
        seriePower.forEach(function(el, index){
            chartObj.series[0].addPoint(el, false, false)
        })
        serieState.forEach(function(el, index){
            chartObj.series[1].addPoint(el, false, false)
        })
        serieStateHuman.forEach(function(el, index){
            chartObj.series[2].addPoint(el, false, false)
        })
        // chartObj.redraw({duration: 1500, easing: 'easeOutBounce'})
        chartObj.redraw()
        if (typeof(cb) == 'function') cb()
    })
}

function drawChart(seriePower, serieState, serieStateHuman, maxPowerValue)
{
    console.log("done processing, start rendering")
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });
    
    // Set chart options
    var options = {
        chart: {
            renderTo: 'chart_div',
            // type: 'line',
            type: 'area',
            
            spacingTop: 10,
            spacingLeft: 10,
            spacingRight: 10,
            //width: null,
            //height: 500,
            zoomType: 'x'
        },
        line: {
            connectNulls : false,
            allowPointSelect : true
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
                month: '%e. %b',
                year: '%b'
            },
            title: {
                text: 'Datetime'
            },
            // ordinal: false
        },
        yAxis: {
            type: 'number',
            title: {
                text: 'Power (kW)'
            },
            min: 0,
            max: maxPowerValue
        },
        rangeSelector : {
            buttons: [{
                type: 'hour',
                count: 1,
                text: '1h'
            }, {
                type: 'day',
                count: 1,
                text: '1d'
            }, {
                type: 'month',
                count: 1,
                text: '1m'
            }, {
                type: 'all',
                text: 'All'
            }],
            inputEnabled: false, // it supports only days
            selected : 50 // all
        },
        scrollbar : {
            enabled : true,
            minWidth: 40
        },
        navigator : {
            //baseSeries: 0,
            series:[{
                data: seriePower,
                    dataGrouping: {
                        enabled : false,
                }
            },{
                data: serieState,
                    dataGrouping: {
                        enabled : false,
                }
            },{
                data: serieStateHuman,
                    dataGrouping: {
                        enabled : false,
                }
            }
            ],
            maskInside: false
        },
        plotOptions: {
            marker: {
                radius: 2
            },
            lineWidth: 1,
            states: {
                hover: {
                    lineWidth: 1
                }
            },
            series:{
                showInNavigator: true
            }
            // series: {
            //     point: {
            //         events: {
            //             click: addEvent
            //         }
            //     }
            // }
        },
        loading: {
            labelStyle: {
                top: '50%',
                left: '50%',
                backgroundImage: 'url("img/loading.gif")',
                backgroundRepeat: 'no-repeat',
                backgroundPosition: '50% 70%',
                display: 'block',
                width: '100px',
                height: '50px',
                backgroundColor: '#d8d8d8',
                borderRadius: '5px',
                padding: '5px'
            }
        },
        series: [{
                name: "Power",
                color: '#4696c7',
                data: seriePower,
                dataGrouping: {
                    enabled : false,
                    approximation: "high",
                    smoothed:false,
                    // groupPixelWidth: 0.0001,
                    units: [
                    // [
                    //     'millisecond', // unit name
                    //     [1, 2, 5, 10, 20, 25, 50, 100, 200, 500] // allowed multiples
                    // ], 
                    [
                        'second',
                        [1, 2, 5]
                    ], [
                        'minute',
                        [1, ]
                    ], 
                    [
                       'hour',
                        [1, 2]
                    ], //[
                    //     'day',
                    //     null
                    // ], [
                    //     'week',
                    //     null
                    // ], [
                    //     'month',
                    //     null
                    // ], [
                    //     'year',
                    //     null
                    // ]
                    ],
                    showInNavigator:true
                },
                point: {
                    events: {
                        click: addEventHandler,
                    }
                },
                marker: {
                    enabled: false,
                    radius: 3
                },
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')],
                        // [0, "#aaf"],
                        // [1, Highcharts.Color("#aaf").setOpacity(0).get('rgba')],
                    ]
                }
            },
            {
                name: "State Changes SAL",
                data: serieState,
                // color: '#9e9e9e',
                dataGrouping: {
                    enabled : false,
                },
                point: {
                    events: {
                        dblclick: removeEventHandler
                    }
                },
                type: 'column',
                // pointWidth: 4,
                //minPointLength: 6,
                minPointLength: maxPowerValue * 0.1,
                showInNavigator:true
            },{
                name: "State Changes Human",
                data: serieStateHuman,
                color: 'green',
                dataGrouping: {
                    enabled : false,
                },
                point: {
                    events: {
                        dblclick: removeEventHumanHandler
                    }
                },
                type: 'column',
                // pointWidth: 4,
                minPointLength: maxPowerValue * 0.1,
                showInNavigator:true
            }
        ]
    }

    $('#chart_div').highcharts('StockChart',options, function(loadedChart){
        chartObj = loadedChart
    })
    // chartObj = $('#chart_div').highcharts()
    
    // chartObj.addSeries({
    //     name: "State Changes SAL",
    //     data: serieState,
    //     // color: '#9e9e9e',
    //     dataGrouping: {
    //         enabled : false,
    //     },
    //     point: {
    //         events: {
    //             dblclick: removeEventHandler
    //         }
    //     },
    //     type: 'column',
    //     // pointWidth: 4,
    //     //minPointLength: 6,
    //     minPointLength: maxPowerValue * 0.1,
    //     showInNavigator:true
    // })

    // chartObj.addSeries({
    //     name: "State Changes Human",
    //     data: serieStateHuman,
    //     color: 'green',
    //     dataGrouping: {
    //         enabled : false,
    //     },
    //     point: {
    //         events: {
    //             dblclick: removeEventHumanHandler
    //         }
    //     },
    //     type: 'column',
    //     // pointWidth: 4,
    //     minPointLength: maxPowerValue * 0.1,
    //     showInNavigator:true
    // })
}

// #######################################
// #######################################
// popUp
// #######################################
function dbClickFunction(){
    console.log('dagsafsdhd')
}


function showStateInput(event) {
    clearDialogInputs()
    $("#point_x").html(event.point.x)
    $("#point_x_datetime").html(new Date(event.point.x))
    $("#point_y").html(event.point.y.toFixed(2))
    $("#vote_transition").css("display", "block")
}

function clearDialogInputs(){
    $('#eventTitle').val("")
    $('#eventDescription').val("")
    $('#eventNrPeople').val("")
}

function getDelta(arr, ini, limit){
    var clickedVal = arr[ini]
    if(arr.length == 0) return 0
    var maxVal = arr[ini]
    var maxIndex = 0
    var minI = Math.max(0, ini - (limit / 2))
    var limitI = Math.min(ini + (limit / 2), arr.length - 1)
    for(var i = minI; i < limitI; i++){
        var item = arr[i]
        if(item > maxVal){
            maxVal = item
            maxIndex = i
        }
        console.log(i, item)
    }
    if(maxIndex > ini){
        return clickedVal - maxVal
    }else{
        return maxVal - clickedVal
    }
}

function shiftChart(startDateString, endDateString){
    var startDate = new Date(startDateString).getTime()
    var endDate = new Date(endDateString).getTime()
    for(var i = 0; i < 3; i++){
    // for(var i = 0; i < chartObj.series.length; i++){
        var serie = chartObj.series[i]
        var nrPoints = serie.data.length
        var indexFirstRem = 0
        var indexLastRem = 0
        for(var j = nrPoints - 1; j > -1 ; j--){
            var point = serie.data[j]
            if(point.x < startDate || point.x >= (endDate + 86400000)){
                indexLastRem = j
                serie.removePoint(point.index,false)
            }
        }
        console.log(indexLastRem)
    }
    chartObj.redraw()
}

function addEventHandler(event, ui){
    var dataObject = this
    var dataGroupLength = 1
    if(dataObject.dataGroup){
        dataGroupLength = dataObject.dataGroup.length
        if(dataObject.dataGroup.length > 3){
            showMessage('Zoom in to avoid time rounding a errors')
            //setExtremes (Number min, Number max, [Boolean redraw], [Mixed animation])
            this.series.chart.xAxis[0].setExtremes (this.x - 60000 * 10, this.x + 60000 * 10, true)
            return;
        }
    }
    
    var dateString = getDateStrFromEpoch(new Date(this.x))
    var dateTimeString = getDateStrFromEpoch(new Date(this.x)) + ' ' + getHourStrFromEpoch(new Date(this.x), true)
    var delta = getDelta(this.series.yData, this.index, 12 * dataGroupLength)
    //this.series.yData[this.index]

    saveEventDB(undefined, dateTimeString, delta, dataObject)
}

function addEventDialog(event, ui){
    var dataObject = this.dataObject
    
    var dateString = getDateStrFromEpoch(new Date(this.x))
    var dateTimeString = getDateStrFromEpoch(new Date(this.x)) + ' ' + getHourStrFromEpoch(new Date(this.x), true)
    var delta = getDelta(this.series.yData, this.index, 12)
    $('#popUpTime').text(getHourStrFromEpoch(new Date(this.x), true))
    $('#popUpPower').text(this.y)

    var yValue = this.y
    $('#newEventDiv').dialog({
        modal: false,
        draggable: true,
        resizable: false,
        //position: ['center', 'center'],   //this does NOT work
        show: 'blind',
        hide: 'blind',
        width: '350px',
        height: 'auto',
        //dialogClass: 'ui-dialog-osx',
        title: 'New Event ' + dateString,
        buttons: [
            { text: 'Cancel', click: function() { $(this).dialog('close') } } ,
            { text: 'Add Event', class:'btnOn', click: function() { saveEventDB($(this).dialog("close"), dateTimeString, delta, dataObject) }} ,
            //{ text: "Remove Event ↑↓", class: 'btnOff', click: function() { saveEventDB($(this, 'remove'), dateTimeString,  dataObject), autofocus: true}} ,
        ],
        open: function(event, ui) {
            //$(".ui-dialog-titlebar-close").hide(); // Hide the [x] button
            $(":button:contains('Add Event')").focus(); // Set focus to the [Ok] button
        }
    });
}

// function editEvent(event, ui){
//     var dataObject = this

//     $('#popUpTime').val(dataObject.title)
//     $('#popUpPower').val(dataObject.description)

//     var dateString = getDateStrFromEpoch(new Date(this.x))

//     $('#newEventDiv').dialog({
//         modal: true,
//         draggable: true,
//         resizable: false,
//         show: 'blind',
//         hide: 'blind',
//         width: 'auto',
//         height: 'auto',
//         title: 'Edit Event - ' + dateString,
//         buttons: [
//             { text: 'Cancel',       click: function() { $(this).dialog("close") }},
//             { text: 'Update Event', click: function() { saveEventDB($(this), dateString, dataObject) }},
//             { text: 'Delete Event', class:'btnOff', click: function() { saveEventDB(function(){$(this).dialog("close")}, dateString, dataObject) }},
//         ],
//         open: function(event, ui) {
//             //$(".ui-dialog-titlebar-close").hide(); // Hide the [x] button
//             $(":button:contains('Remove Event')").focus(); // Set focus to the [Ok] button
//         }
//     })
// }

function removeEventHandler(event, ui){
    var dataObject = this

    var dateString = getDateStrFromEpoch(new Date(this.x))
    var dateTimeString = getDateStrFromEpoch(new Date(this.x)) + ' ' + getHourStrFromEpoch(new Date(this.x), true)   
    removeEventDB(undefined, 'delete', dateTimeString, dataObject, false)        
}

function removeEventHumanHandler(event, ui){
    var dataObject = this
    
    var dateString = getDateStrFromEpoch(new Date(this.x))
    var dateTimeString = getDateStrFromEpoch(new Date(this.x)) + ' ' + getHourStrFromEpoch(new Date(this.x), true)   
    removeEventDB(undefined, 'delete', dateTimeString, dataObject, true)
}

function saveEventDB(cb, dateString, delta, pointClicked){
    // var title = $('#eventTitle').val()
    // var description = $('#eventDescription').val()
    // var nrPeople = $('#eventNrPeople').val()

    var eventObj = {
        testId: $('#selectTest option:selected')[0].value,
        plug_add: $('#selectPlug option:selected')[0].value,
        timestamp: dateString,
        power: pointClicked.y,
        delta: delta
    }

    $.post('http://aveiro.m-iti.org:8000/api/power-event/ds/'+ table_sufix + '/add/plug_add/' + eventObj.plug_add, eventObj, function(data) {
        // if(data.redirect != undefined){
        //     window.location = data.redirect
        //     return;
        // }

        // 2 -> means there was an update of the action and nr_add
        // 1 -> new insert
        if(data.nr_rows == 0) // set to same values
            return;

        eventObj.id = data.insertedId
        var eventChart = $('#chart_div').highcharts()
        var previousExtremes0 = chartObj.xAxis[0].getExtremes()
        var previousExtremes1 = chartObj.xAxis[1].getExtremes()
        eventChart.series[2].addPoint({
            x: Date.parse(eventObj.timestamp),
            y: pointClicked.y + Math.abs(delta),
            z: 0,
            dataObject: eventObj,
            id: data.insertedId
        }, true)
        chartObj.xAxis[0].setExtremes(previousExtremes0.min, previousExtremes0.max)
        chartObj.xAxis[1].setExtremes(previousExtremes1.min, previousExtremes1.max)

        pointClicked.dataObject = eventObj
        showMessage('Added Successfully')
    }).fail(function(err, res) {
        showMessage(JSON.parse(err.responseText).error)
    }).always(function(){
        if(cb)
            cb()
    })
}

function removeEventDB(dialogObj, action, dateString, pointClicked, human){
    // var title = $('#eventTitle').val()
    // var description = $('#eventDescription').val()
    // var nrPeople = $('#eventNrPeople').val()
    if(human == undefined || human == false) {
        human = 'sal'
    }else{
        human = 'human'
    }

    var eventObj = {
        testId: $('#selectTest option:selected')[0].value,
        plug_add: $('#selectPlug option:selected')[0].value,
        action: action,
        timestamp: dateString,
        power: pointClicked.y,
        delta: popUpFinalPower
    }

    var newActive = pointClicked.active != 0 ? 0 : 1
    $.get('http://aveiro.m-iti.org:8000/api/power-event-human/edit/ds/' + table_sufix + '/active/' + newActive + '/' + human + '/id/' + pointClicked.id, function(data) {
        var previousExtremes0 = chartObj.xAxis[0].getExtremes()
        var previousExtremes1 = chartObj.xAxis[1].getExtremes()
        if(human == 'human'){
            pointClicked.remove()
            showMessage('Removed Successfully')
        }else{
            pointClicked.active = newActive
            if(newActive == 0){     //it has just been disabled
                pointClicked.color = colors.GREY
                pointClicked.update({ marker: { fillColor: colors.GREY }} )
                pointClicked.graphic.attr({ fill: colors.GREY })
                showMessage('Disabled Successfully')
            }else{
                pointClicked.color = colors.RED
                pointClicked.update({ marker: { fillColor: colors.RED}} )
                pointClicked.graphic.attr({ fill: colors.RED })
                showMessage('Enabled Successfully')
            }
            //pointClicked.series.data[pointClicked.index].graphic.attr({ fill: 'red' })
        }
        chartObj.xAxis[0].setExtremes(previousExtremes0.min, previousExtremes0.max)
        chartObj.xAxis[1].setExtremes(previousExtremes1.min, previousExtremes1.max)
        // var eventChart = $('#chart_div').highcharts()
        // eventChart.series[2].removePoint(pointClicked.index, true)
    }).fail(function(err, res) {
        showMessage(JSON.parse(err.responseText).error)
    }).always(function(){
        if(dialogObj != undefined) dialogObj.dialog("close")
    })
}

function showMessage(text, className){
    if(className == undefined){
        className = "greenMessage"
    }
    $('#messageLabel').text(text)
    $('#messageLabel').removeClass();
    $('#messageLabel').addClass(className)
    setTimeout(function(){$('#messageLabel').text('')}, 3000)
}
