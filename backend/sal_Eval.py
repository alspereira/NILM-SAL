# get the devices

# for each device 

# get the tests

from sal_DB import *
import numpy as np

def countExact(data):
    exact = 0
    for item in data:
        exact = exact + item['exact']
    return exact

table_sufix = "H1"
tol = 2

plugs = readPlug(table_sufix)
tests = readTest()

for plug in plugs:
    GT = getGroundTruth(table_sufix, plug['id'])
    print("GT %s: %i") %(plug['appliance_name'], len(GT))
    for test in tests:
        DE = readSystemEvent(table_sufix, plug['id'], test['id']) 
        TP = getTP(table_sufix, plug['id'], test['id'], tol)
        exact = countExact(TP)
        num_FP = len(DE) - len(TP)
        num_FN = len(GT) - len(TP)
        print("TEST %i: DE %i , TP %i, FP %i, FN %i, Exact TP %i ") %(test['id'], len(DE), len(TP),num_FP, num_FN, exact)
        createEvaluation(table_sufix, plug['id'], test['id'], tol, len(TP), num_FP, num_FN, exact)


