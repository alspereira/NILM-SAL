# creates tests 
from sal_DB import *

# tests have 4 parameters, samples before, samples after, min delta and min elpased time

def fixedSamples():
    pre = [1] # in samples
    post = [1]
    deltas = [0.10, 0.25, 0.5, 0.75] # in percentage
    t_elap = [2, 5, 10, 15, 30, 60] # in seconds

    for pr in pre:
        for po in post:
            for d in deltas:
                for t in t_elap:
                    name = '%i_%i_%i_%i' %(pr, po, d*100, t)
                    lid = createTest(name, pr, po, d, t)
                    print(lid)

fixedSamples()
    