# runs the detection algorithms

# get the appliances
# get the tests

from sal_DB import *
from itertools import islice
from datetime import datetime
import numpy as np

def runDetectors(table_sufix):
    tests = readTest()
    plugs = readPlug(table_sufix)

    for p in plugs:
        data = readSamplesByMacAddress(table_sufix, p['id'])
        np_data = np.asarray(data)
        print('plug: %i %s' %(p['id'], p['appliance_name']))
        for t in tests:
            #delta = p['max_power'] - p['min_power']
            delta = p['mode_bin_x']
            power = t['pthr'] * delta
            detectEvent(t['id'], p['id'], np_data, t['wpre'], t['wpost'], power, t['telap'], table_sufix)

def window(iterable, size=5):
	it = iter(iterable)
	result = tuple(islice(it,size))
	if len(result) == size:
		yield result    
	for elem in it:
		result = result[1:] + (elem,)
		yield result

def detectEvent(test_id, mac_address, data, wpre, wpost, pthr, telap, table_sufix):
	print('wpre: %i wpost: %i pthr: %f telap: %i' %(wpre, wpost, pthr, telap))
	wins = window(data, wpre+wpost+1)
	current_sample_time = 0
	last_event_time = datetime(1970,1,1,0,0,0)

	for item in wins:
		t = np.asarray(item)[:,0]
		p = np.asarray(item)[:,1]

		u0 = np.sum(p[0:wpre])
		u1 = np.sum(p[wpre+1:])
		diff = u1 - u0
		current_sample_time = t[wpre]
		time_diff = (current_sample_time - last_event_time).total_seconds()

		if(abs(diff)>pthr and time_diff > telap):
			#print('event at %s with delta of %f and a time diff of %i seconds' % (t[wpre], diff, time_diff))
			last_event_time = current_sample_time
			createSystemLabel(table_sufix, test_id, mac_address, last_event_time , u0, diff)

runDetectors('H1')