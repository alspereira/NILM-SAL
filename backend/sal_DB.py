from connection_info import *
import MySQLdb
import itertools

def get_db_connection():
    return MySQLdb.connect(user = db['username'],passwd = db['password'],host = db['host'],db = db['db'])

def dictfetchall(cursor):
    """Returns all rows from a cursor as a list of dicts"""
    desc = cursor.description
    return [dict(itertools.izip([col[0] for col in desc], row)) 
            for row in cursor.fetchall()]

def getPlugById(table_sufix, plug_id):
    conn = get_db_connection()
    cursor = conn.cursor()
    print(plug_id)
    sql = 'SELECT * FROM device_%s WHERE id = %s' %(table_sufix, plug_id)
    
    nrows = cursor.execute(sql)
    data = dictfetchall(cursor)
    
    return data

def readPlug(table_sufix):
    conn = get_db_connection()
    cursor = conn.cursor()
    sql = 'SELECT * FROM device_%s' %(table_sufix)
    nrows = cursor.execute(sql)
    data = dictfetchall(cursor)
    return data

def createTest(name, wpre, wpost, pthr, telap):
    conn = get_db_connection()
    query = conn.cursor()
    sql = "INSERT INTO test (name, wpre, wpost, pthr, telap) VALUES %(values)s"
    values = '("%s",%i,%i,%f,%i)' % (name, wpre, wpost, pthr, telap)
    print(values)
    sql = sql % {'values': values}
    print(sql)
    nrows = query.execute(sql % {'values': values})
    inserted_id = query.lastrowid
    print 'rows', nrows, 'inserted_id', inserted_id
    conn.commit()
    query.close()
    conn.close()
    return inserted_id

def readTest():
    conn = get_db_connection()
    cursor = conn.cursor()
    sql = 'SELECT * FROM test'
    nrows = cursor.execute(sql)
    data = dictfetchall(cursor)
    return data

def readSamplesByMacAddress(table_sufix, mac_address):
    conn = get_db_connection()
    query = conn.cursor()
    sql = 'SELECT timestamp, power from ground_truth_%s WHERE mac_address = "%s"' % (table_sufix, mac_address)
    nrows = query.execute(sql)
    rows = query.fetchall()
    query.close()
    conn.close()
    return rows

def createSystemLabel(table_sufix, test_id, mac_address, timestamp, initial_power, delta, active = 1):
    conn = get_db_connection()
    query = conn.cursor()
    sql = 'INSERT INTO power_event_system_%s (test_id, mac_address, timestamp, initial_power, delta, active) VALUES %s'
    values = '(%i, "%s","%s",%f,%f,%i)' % (test_id, mac_address, timestamp, initial_power, delta, active)
    sql = sql %(table_sufix, values)

    #print sql
    nrows = query.execute(sql)
    inserted_id = query.lastrowid
    #print 'rows', nrows, 'inserted_id', inserted_id
    conn.commit()
    query.close()
    conn.close()
    return inserted_id

def getGroundTruth(table_sufix, mac_add):
    conn = get_db_connection()
    cursor = conn.cursor()
    sql = "SELECT * FROM power_event_gt_%s WHERE mac_address = %s" %(table_sufix, mac_add)
    nrows = cursor.execute(sql)
    data = dictfetchall(cursor)
    
    return data

def getTP(table_sufix, mac_add, test_id, tolerance):
    conn = get_db_connection()
    cursor = conn.cursor()
    sql = """SELECT DISTINCT TP.timestamp, TP.exact FROM 
    (SELECT GT.timestamp, if(GT.timestamp = DE.timestamp,1, 0) as exact 
    FROM power_event_gt_%(table_sufix)s AS GT 
    JOIN power_event_system_%(table_sufix)s as DE
	ON 
    GT.mac_address = DE.mac_address
	AND GT.timestamp - Interval %(tolerance)i second <= DE.timestamp
	AND GT.timestamp + Interval %(tolerance)i second >= DE.timestamp
	AND test_id = %(test_id)i 
    WHERE
	GT.mac_address = %(mac_add)s) AS TP"""
    sql = sql %{"table_sufix":table_sufix, "tolerance":tolerance, "test_id":test_id, "mac_add": mac_add}

    nrows = cursor.execute(sql)
    data = dictfetchall(cursor)
    
    return data

def readSystemEvent(table_sufix, mac_add, test_id):
    conn = get_db_connection()
    cursor = conn.cursor()
    sql = "SELECT * from power_event_system_%s WHERE mac_address = %s and test_id = %i" %(table_sufix, mac_add, test_id)
    nrows = cursor.execute(sql)
    data = dictfetchall(cursor)
    
    return data

def createEvaluation(table_sufix, mac_add, test_id, tol, tp, fp, fn, exact):
    conn = get_db_connection()
    query = conn.cursor()
    sql = 'INSERT INTO evaluation_%s (mac_address, test_id, tolerance, TP, FP, FN, exact) VALUES %s'
    values = '("%s", %i, %i, %i, %i, %i, %i)' % (mac_add, test_id, tol, tp, fp, fn, exact)
    sql = sql %(table_sufix, values)

    rows = query.execute(sql)
    inserted_id = query.lastrowid
    #print 'rows', nrows, 'inserted_id', inserted_id
    conn.commit()
    query.close()
    conn.close()
    return inserted_id