from itertools import islice
import numpy as np

def split_every(n, iterable):
    i = iter(iterable)
    piece = tuple(islice(i, n))
    while piece:
        yield piece
        piece = tuple(islice(i, n))

def ssd_simple(data, size=5, stdev=1):
    out = []
    d = split_every(size, data)
    print(d)
    for w in d:
        p = np.asarray(w)[:,1]
        _stdev = np.std(p)
        if _stdev > stdev:
            for i in w:
                out.append(i)
    return out

# find the stdev of all the windows and keep those above stdev
def ssd(data, size=5, stdev=1):
    out = []
    d = split_every(size, data)
    print(d)
    last_added = False
    index = 0
    
    for w in d:
        if index == 0:
            previous = w
            for i in w:
                out.append(i)
            last_added = True
            index = index + 1
            print('just added the first element')
            continue
        p = np.asarray(w)[:,1]
        _stdev = np.std(p)
        if _stdev > stdev:
            if last_added == False:
                for i in previous:
                    out.append(i)
            for i in w:
                out.append(i)
            last_added = True
        else:
            last_added = False
        previous = w
        index = index + 1
    if(last_added == False):
        for i in previous:
                out.append(i)
        print('just added the last element')
    else:
        print('last element not necessary')

    return out

#data = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
#np_data = np.asarray(data)
