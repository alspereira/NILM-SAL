#!/usr/bin/python

# from tornado.wsgi import WSGIContainer
# from tornado.httpserver import HTTPServer
# from tornado.ioloop import IOLoop

from gevent.wsgi import WSGIServer
#from yourapplication import app

from flask import Flask, jsonify, request, Response, make_response
from flask_cors import CORS, cross_origin
from connection_info import *
from sal_SSD import *
from sal_DB import *
import MySQLdb
import json
import requests
#import mycrypt as Crypto
# import mycryptECB as CryptoECB
import itertools
import datetime
import numpy as np


#http://aveiro.m-iti.org:8000/api/sample/plug_add/000D6F000099265F/startDate/2016-12-01/endDate/2016-12-01
#http://aveiro.m-iti.org:8000/api/event/test/1/plug_add/000D6F000099265F/startDate/2016-12-01/endDate/2016-12-01
#http://aveiro.m-iti.org:8000/api/event-human/test/1/plug_add/000D6F000099265F/startDate/2016-12-01/endDate/2016-12-01
#http://aveiro.m-iti.org:8000/api/get-plugs
#http://aveiro.m-iti.org:8000/api/get-tests

def get_db_connection():
    return MySQLdb.connect(user = db['username'],passwd = db['password'],host = db['host'],db = db['db'])

def dictfetchall(cursor):
    """Returns all rows from a cursor as a list of dicts"""
    desc = cursor.description
    return [dict(itertools.izip([col[0] for col in desc], row)) 
            for row in cursor.fetchall()]
    
# def redirectCopy(jsonValues, BASE_URL, BOX_ID, table):
#     jsonStr = json.dumps(jsonValues)
    
#     if not BASE_URL.endswith("/"):
#         BASE_URL += '/'
#     composedUrl = BASE_URL + str(BOX_ID) + '/' + table
#     try:
#         headers = {'Content-type': 'application/json'}
#         r = requests.post(composedUrl, data=jsonStr, headers=headers)
#     except:
#         pass
    
# def ensure_db_tables():
#     conn = get_db_connection()
#     query = conn.cursor()

#     sql = """CREATE TABLE IF NOT EXISTS rawSample
#                 (
#                     id INT NOT NULL AUTO_INCREMENT,
#                     raw0 FLOAT,
#                     raw1 FLOAT,
#                     raw2 FLOAT,
#                     raw3 FLOAT,
#                     raw4 FLOAT,
#                     raw5 FLOAT,
#                     raw6 FLOAT,
#                     capture_timestamp DATETIME,
#                     PRIMARY KEY (id)
#                 );"""
#     query.execute(sql)

#     sql = """CREATE TABLE IF NOT EXISTS radiation
#                  (
#                      id INT unsigned NOT NULL AUTO_INCREMENT,
#                      box_id INT unsigned DEFAULT NULL,
#                      raw_value FLOAT unsigned DEFAULT NULL,
#                      capture_timestamp DATETIME,
#                      PRIMARY KEY (id)
#                  );"""
#     query.execute(sql)

#     sql = """CREATE TABLE IF NOT EXISTS sample
#                  (
#                      id INT unsigned NOT NULL AUTO_INCREMENT,
#                      box_id INT unsigned DEFAULT NULL,
#                      impulse_prod INT unsigned DEFAULT NULL,
#                      kw_prod FLOAT unsigned DEFAULT NULL,
#                      impulse_cons INT unsigned DEFAULT NULL,
#                      kw_cons FLOAT unsigned DEFAULT NULL,
#                      voltage FLOAT unsigned DEFAULT NULL,
#                      capture_timestamp DATETIME,
#                      PRIMARY KEY (id)
#                  );"""
#     query.execute(sql)

#     query.close()
#     conn.close()

app = Flask(__name__)
CORS(app)

# @app.route('/api/raw/sample/', methods=['POST'])
# def post_raw_sample():
#     if request.json:
#         conn = get_db_connection()
#         query = conn.cursor()
#         sql = "INSERT INTO rawSample (raw0, raw1, raw2, raw3, raw4, raw5, raw6, timestamp) VALUES %(values)s;"
#         values = ",".join(["(%f,%f,%f,%f,%f,%f,%f,'%s')" % (
#                                                 float(row['raw0']),
#                                                 float(row['raw1']),
#                                                 float(row['raw2']),
#                                                 float(row['raw3']),
#                                                 float(row['raw4']),
#                                                 float(row['raw5']),
#                                                 float(row['raw6']),
#                                                 conn.escape_string(row['timestamp']),) for row in request.json])
#         nrows = query.execute(sql % {'values': values})
#         conn.commit()
#         query.close()
#         conn.close()
#         return jsonify({'rows':nrows})
#     return make_response("error", 400)

# @app.route('/api/raw/sample/get', methods=['GET'])
# def get_raw_sample():
#     sql = "SELECT * FROM rawSample"
#     conn = get_db_connection()
#     query = conn.cursor()
#     nrows = query.execute(sql)
#     rows = query.fetchall()
#     data = "id,raw0,raw1,raw2,raw3,raw4,raw5,raw6,timestamp\n" + "\n".join([','.join(map(str, row)) for row in rows])
#     query.close()
#     conn.close()
#     response = Response(data, mimetype='text/csv')
#     response.headers['Content-Disposition'] = 'attachment; filename=rawSamples.csv'
#     return response

# GET AND POST for Radiation
# @app.route('/api/box/<int:box_id>/radiation', methods=['GET'])
# @app.route('/api/box/<int:box_id>/radiation/<int:limit>', methods=['GET'])
# @app.route('/api/box/<int:box_id>/radiation/<int:limit>/<path:dateStart>', methods=['GET'])
# @app.route('/api/box/<int:box_id>/radiation/<int:limit>/<path:dateStart>/<path:dateEnd>', methods=['GET'])
# def get_radiation(box_id, limit=0, dateStart="none", dateEnd="none"):
#     conn = get_db_connection()
#     query = conn.cursor()

#     sql = "SELECT * FROM radiation WHERE box_id=%i" % (box_id,)
#     if(dateStart != "none"):
#         sql += " AND capture_timestamp >= '" + conn.escape_string(dateStart) + "'"
#     if(dateEnd != "none"):
#         dateEnd += " 23:59:59"
#         sql += " AND capture_timestamp <= '" + conn.escape_string(dateEnd) + "'"
#     if(limit != 0):
#         sql += " Limit " + str(limit)

#     nrows = query.execute(sql)
#     rows = query.fetchall()
#     data = "id,box_id,raw_value,capture_timestamp\n" + "\n".join([','.join(map(str, row)) for row in rows])
#     query.close()
#     conn.close()
#     response = Response(data, mimetype='text/csv')
#     response.headers['Content-Disposition'] = 'attachment; filename=radiation_%i.csv' % (box_id,)
#     return response

# @app.route('/api/box/<int:box_id>/radiation', methods=['POST'])
# def post_radiation(box_id):
#     decrypted = Crypto.decrypt(request.data, ENCRYPTION_KEY)
#     try:
#         jsonObj = json.loads(decrypted)
#         if jsonObj:
#             conn = get_db_connection()
#             query = conn.cursor()
#             sql = "INSERT INTO radiation (box_id, raw_value, capture_timestamp) VALUES %(values)s;"
#             values = ",".join(["(%i,%f,'%s')" % (box_id,
#                                                  float(row['raw_value']),
#                                                  conn.escape_string(row['capture_timestamp']),) for row in jsonObj])
#             nrows = query.execute(sql % {'values': values})
#             conn.commit()
#             query.close()
#             conn.close()
#             return jsonify({'rows':nrows})
#         return make_response("error: bad json", 400)
#     except: return make_response("error: wrong encryption key", 400)

# GET sample
@app.route('/api/sample/ds/<string:table_sufix>/plug_add/<path:mac_add>/startDate/<path:dateStart>/endDate/<path:dateEnd>', methods=['GET'])
def get_sample(table_sufix, mac_add, dateStart="none", dateEnd="none"):
    conn = get_db_connection()
    query = conn.cursor()

    sql = 'SELECT timestamp,power FROM ground_truth_%s WHERE mac_address="%s" ' % (conn.escape_string(table_sufix), conn.escape_string(mac_add),)

    if(dateStart != "none"):
        sql += " AND timestamp >= '" + conn.escape_string(dateStart) + "'"
    if(dateEnd != "none"):
        dateEnd += " 23:59:59"
        sql += " AND timestamp <= '" + conn.escape_string(dateEnd) + "'"

    sql += " ORDER BY timestamp"
    print sql
    nrows = query.execute(sql)
    #rows = dictfetchall(query)
    rows = query.fetchall()
    query.close()
    conn.close()
    rows2 = []
    last_item = [0,-1]
    last_equal = False
    for item in rows:
        if item[1] == last_item[1]:
            last_equal = True
            last_item = item
            continue
        else:
            if last_equal:
                item2 = list(last_item)
                item2[0] = item2[0].strftime('%Y-%m-%d %H:%M:%S')
                rows2.append(item2)
            last_equal = False
        last_item = item

        item2 = list(item)
        item2[0] = item2[0].strftime('%Y-%m-%d %H:%M:%S')
        rows2.append(item2)
    data = json.dumps(rows2)
    response = Response(data, mimetype='application/json')
    response.headers['Content-Disposition'] = 'attachment; filename=sample_%s.json' % (mac_add,)
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response

@app.route('/api/sample_ssd/ds/<string:table_sufix>/plug_add/<path:mac_add>/startDate/<path:dateStart>/endDate/<path:dateEnd>', methods=['GET'])
def get_sample_ssd(table_sufix, mac_add, dateStart="none", dateEnd="none"):
    conn = get_db_connection()
    query = conn.cursor()

    sql = 'SELECT timestamp,power FROM ground_truth_%s WHERE mac_address="%s" ' % (conn.escape_string(table_sufix), conn.escape_string(mac_add),)

    if(dateStart != "none"):
        sql += " AND timestamp >= '" + conn.escape_string(dateStart) + "'"
    if(dateEnd != "none"):
        dateEnd += " 23:59:59"
        sql += " AND timestamp <= '" + conn.escape_string(dateEnd) + "'"

    sql += " ORDER BY timestamp"
    print sql
    nrows = query.execute(sql)
    rows = query.fetchall()
    row_count = query.rowcount
    query.close()
    conn.close()

    # filter this shit
    print(row_count)
    if row_count > 0:
        plug_data = getPlugById(table_sufix, mac_add)
        print(plug_data[0])
        delta = plug_data[0]['max_power']-plug_data[0]['min_power']
        delta = 0.1 * delta
        print('delta: %f' %(delta))
        mode = plug_data[0]['mode_bin_x']
        mode = 0.1 * mode
        print('mode: %f' %(mode))
        out_2 = ssd(rows,30,delta)
        out = ssd(rows, 30, mode)
    else:
        out = []
        out_2 = []
    #out = rows
    #out = out.tolist()
    #out = rows
    rows2 = []
    print('raw samples out %i' %(len(rows)))
    print('delta samples out %i' %(len(out_2)))
    print('mode samples out %i' %(len(out)))
    for item in out:
        item2 = list(item)
        #print(type(item))
        #print(item)
        #item2 = item
        item2[0] = item2[0].strftime('%Y-%m-%d %H:%M:%S')
        rows2.append(item2)
    data = json.dumps(rows2)

    #print(data)
    response = Response(data, mimetype='application/json')
    response.headers['Content-Disposition'] = 'attachment; filename=sample_%s.json' % (mac_add,)
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response

# GET SAL events
@app.route('/api/event/ds/<string:table_sufix>/test/<int:testId>/plug_add/<string:mac_add>/startDate/<path:dateStart>/endDate/<path:dateEnd>', methods=['GET'])
def get_event(table_sufix, testId, mac_add, dateStart="none", dateEnd="none"):
    conn = get_db_connection()
    query = conn.cursor()

    sql = 'SELECT id,test_id,timestamp,initial_power,delta,active FROM power_event_system_%s WHERE mac_address="%s" AND test_id=%i ' % (conn.escape_string(table_sufix), conn.escape_string(mac_add),testId)

    if(dateStart != "none"):
        sql += " AND timestamp >= '" + conn.escape_string(dateStart) + "'"
    if(dateEnd != "none"):
        dateEnd += " 23:59:59"
        sql += " AND timestamp <= '" + conn.escape_string(dateEnd) + "'"

    sql += " ORDER BY timestamp"
    print sql
    nrows = query.execute(sql)
    #rows = dictfetchall(query)
    rows = query.fetchall()
    query.close()
    conn.close()
    rows2 = []
    for item in rows:
        item2 = list(item)
        item2[2] = item2[2].strftime('%Y-%m-%d %H:%M:%S')
        rows2.append(item2)
    data = json.dumps(rows2)
    response = Response(data, mimetype='application/json')
    response.headers['Content-Disposition'] = 'attachment; filename=sample_%s.json' % (mac_add,)
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response

# GET USER EVENTS
@app.route('/api/event-human/ds/<string:table_sufix>/test/<int:testId>/plug_add/<path:mac_add>/startDate/<path:dateStart>/endDate/<path:dateEnd>', methods=['GET'])
def get_event_human(table_sufix, testId, mac_add, dateStart="none", dateEnd="none"):
    conn = get_db_connection()
    query = conn.cursor()

    sql = 'SELECT id,test_id,timestamp,initial_power,delta,active,nr_add,nr_rem FROM power_event_human_%s WHERE mac_address="%s" AND test_id=%i ' % (conn.escape_string(table_sufix), conn.escape_string(mac_add), testId)

    if(dateStart != "none"):
        sql += " AND timestamp >= '" + conn.escape_string(dateStart) + "'"
    if(dateEnd != "none"):
        dateEnd += " 23:59:59"
        sql += " AND timestamp <= '" + conn.escape_string(dateEnd) + "'"

    sql += " ORDER BY timestamp"
    print sql
    nrows = query.execute(sql)
    #rows = dictfetchall(query)
    rows = query.fetchall()
    query.close()
    conn.close()
    rows2 = []
    for item in rows:
        item2 = list(item)
        item2[2] = item2[2].strftime('%Y-%m-%d %H:%M:%S')
        rows2.append(item2)
    data = json.dumps(rows2)
    response = Response(data, mimetype='application/json')
    response.headers['Content-Disposition'] = 'attachment; filename=sample_%s.json' % (mac_add,)
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response

#POWER EVENTS POST
@app.route('/api/power-event/ds/<string:table_sufix>/add/plug_add/<path:mac_add>', methods=['POST'])
def post_power_event(table_sufix, mac_add):
    #print request.values
    #print request.values.to_dict()
    try:
    #jsonObj = json.loads(dict(request.values))     # will thow error if not json (if the key was wrong) not prob, flask will reload itself
        jsonObj = request.values.to_dict()
        #print jsonObj
        if jsonObj:
            conn = get_db_connection()
            query = conn.cursor()
            sql = "INSERT INTO power_event_human_%(table_sufix)s (test_id, active, mac_address, timestamp, initial_power, delta) VALUES %(values)s ON DUPLICATE KEY UPDATE nr_add = nr_add + if(active = 0, 1, 0), active=1"
            values = '(%i,"%i","%s","%s",%f,%f)' % (
                int(jsonObj['testId']),
                1,
                conn.escape_string(mac_add),
                conn.escape_string(jsonObj['timestamp']),
                float(jsonObj['power']),
                float(jsonObj['delta'])
            )
            print sql

            nrows = query.execute(sql % {'table_sufix':table_sufix, 'values': values})
            inserted_id = query.lastrowid
            print 'rows', nrows, 'inserted_id', inserted_id
            conn.commit()
            query.close()
            conn.close()
            #redirectCopy(jsonObj, "http://li1097-240.members.linode.com:8100/api/box/", box_id, "sample")
            return jsonify({'insertedId': inserted_id,'nr_rows':nrows})
        return make_response("error: bad json", 400)
    except Exception as excpt:
        print type(excpt)     # the exception instance -> IntegrityError
        print excpt 
        if excpt[0] == 1062:    #mysql error code for duplicates
            return make_response('{"error": "Duplicate event"}', 400)
        else:
            return make_response('{"error": "unknown"}', 400)


# @app.route('/api/box/<int:box_id>/sample', methods=['POST'])
# def post_sample(box_id):
#     decrypted = Crypto.decrypt(request.data, ENCRYPTION_KEY)
#     try:
#         jsonObj = json.loads(decrypted)     # will thow error if not json (if the key was wrong) not prob, flask will reload itself
#         if jsonObj:
#             conn = get_db_connection()
#             query = conn.cursor()
#             sql = "INSERT INTO sample (box_id, impulse_prod, kw_prod, impulse_cons, kw_cons, voltage, wflow_ticks, lpm, capture_timestamp) VALUES %(values)s;"
#             values = ",".join(["(%i,%i,%f,%i,%f,%f,%i,%f,'%s')" % (box_id,
#                                                     int(row['impulse_prod']),
#                                                     float(row['kw_prod']),
#                                                     int(row['impulse_cons']),
#                                                     float(row['kw_cons']),
#                                                     float(row['voltage']),
#                                                     int(row['wflow_ticks']),
#                                                     float(row['lpm']),
#                                                     conn.escape_string(row['capture_timestamp']),) for row in jsonObj])
#             nrows = query.execute(sql % {'values': values})
#             conn.commit()
#             query.close()
#             conn.close()
#             #redirectCopy(jsonObj, "http://li1097-240.members.linode.com:8100/api/box/", box_id, "sample")
#             return jsonify({'rows':nrows})
#         return make_response("error: bad json", 400)
#     except: return make_response("error: wrong encryption key", 400)

# @app.route('/api/get-active/minutes/<int:minutes>', methods=['GET'])
# def get_active(minutes):
#     conn = get_db_connection()
#     cursor = conn.cursor()

#     #sql = "SELECT * FROM sample WHERE capture_tim=%i" % (box_id,)
#     sql = "select id, box_id, impulse_prod, kw_prod, impulse_cons, kw_cons, voltage, wflow_ticks, lpm, capture_timestamp from sample where capture_timestamp > NOW() - interval " + conn.escape_string(str(minutes)) + " minute group by box_id"
    
#     nrows = cursor.execute(sql)
#     data = dictfetchall(cursor)
    
#     cursor.close()
#     conn.close()
#     #response = Response(data, mimetype='text/json')
#     #response.headers['Content-Disposition'] = 'attachment; filename=sample_%i.csv' % (box_id,)
#     for item in data:
#         item["capture_timestamp"] = item["capture_timestamp"].strftime('%Y-%m-%d %H:%M:%S')

#     return json.dumps(data)#response
  
#################################### 
#################################### 
# REMOVE HUMAN EVENT
#################################### 
@app.route('/api/power-event-human/edit/ds/<string:table_sufix>/active/<int:active_val>/<path:event_origin>/id/<int:event_id>', methods=['GET'])
def remove_event_human(table_sufix, active_val, event_origin, event_id):
    conn = get_db_connection()
    cursor = conn.cursor()

    print event_origin
    if event_origin == 'sal':
        if active_val != 0:
            sql = 'UPDATE power_event_system_%s SET active=%i, nr_add=nr_add + 1 WHERE id = %i' % (table_sufix, active_val, event_id)
        else:
            sql = 'UPDATE power_event_system_%s SET active=%i, nr_rem=nr_rem + 1 WHERE id = %i' % (table_sufix, active_val, event_id)
    else:
        if active_val != 0:
            sql = 'UPDATE power_event_human_%s SET active=%i, nr_add=(nr_add + 1) WHERE id = %i' % (table_sufix, active_val, event_id)
        else:
            sql = 'UPDATE power_event_human_%s SET active=%i, nr_rem=(nr_rem + 1) WHERE id = %i' % (table_sufix, active_val, event_id)


    print sql
    nrows = cursor.execute(sql)
    data = dictfetchall(cursor)
    conn.commit()
    cursor.close()
    conn.close()
    #response = Response(data, mimetype='text/json')
    #response.headers['Content-Disposition'] = 'attachment; filename=sample_%i.csv' % (box_id,)
    response = Response(json.dumps(data), mimetype='application/json')
    return response


#################################### 
#################################### 
# GET TESTS AND EVENTS
#################################### 

@app.route('/api/get-datasets', methods=['GET'])
def get_datasets():
    conn = get_db_connection()
    cursor = conn.cursor()

    sql = 'SELECT dataset, sufix FROM dataset order by id asc'
    
    nrows = cursor.execute(sql)
    data = dictfetchall(cursor)
    
    cursor.close()
    conn.close()

    response = Response(json.dumps(data), mimetype='application/json')
    return response

@app.route('/api/get-plugs/<string:table_sufix>', methods=['GET'])
def get_plugs_new(table_sufix):
    conn = get_db_connection()
    cursor = conn.cursor()

    sql = 'SELECT id, appliance_name FROM device_%s ORDER BY appliance_name ASC' %(table_sufix)
    
    nrows = cursor.execute(sql)
    data = dictfetchall(cursor)
    
    cursor.close()
    conn.close()
    response = Response(json.dumps(data), mimetype='application/json')
    return response

@app.route('/api/get-plugs', methods=['GET'])
def get_plugs():
    conn = get_db_connection()
    cursor = conn.cursor()


    sql = 'SELECT * FROM room__plug ORDER BY name'
    
    nrows = cursor.execute(sql)
    data = dictfetchall(cursor)
    
    cursor.close()
    conn.close()
    #response = Response(data, mimetype='text/json')
    #response.headers['Content-Disposition'] = 'attachment; filename=sample_%i.csv' % (box_id,)
    response = Response(json.dumps(data), mimetype='application/json')
    return response

@app.route('/api/get-tests', methods=['GET'])
def get_tests():
    conn = get_db_connection()
    cursor = conn.cursor()

    #sql = "SELECT * FROM sample WHERE capture_tim=%i" % (box_id,)
    sql = 'SELECT * FROM test'
    
    nrows = cursor.execute(sql)
    data = dictfetchall(cursor)
    
    cursor.close()
    conn.close()
    #response = Response(data, mimetype='text/json')
    #response.headers['Content-Disposition'] = 'attachment; filename=sample_%i.csv' % (box_id,)
    response = Response(json.dumps(data), mimetype='application/json')
    return response

@app.route('/api/get-tests/ds/<string:table_sufix>', methods=['GET'])
def get_testsByDataset(table_sufix):
    conn = get_db_connection()
    cursor = conn.cursor()

    #sql = "SELECT * FROM sample WHERE capture_tim=%i" % (box_id,)
    #sql = 'SELECT * FROM test'
    sql = "SELECT DISTINCT test_id from power_event_system_%s ORDER BY test_id ASC" %(table_sufix)
    print(sql)
    nrows = cursor.execute(sql)
    data = dictfetchall(cursor)
    
    cursor.close()
    conn.close()
    #response = Response(data, mimetype='text/json')
    #response.headers['Content-Disposition'] = 'attachment; filename=sample_%i.csv' % (box_id,)
    response = Response(json.dumps(data), mimetype='application/json')
    return response

@app.route('/api/get-tests/ds/<string:table_sufix>/plug_add/<string:mac_add>', methods=['GET'])
def get_testsByMacAddress(table_sufix, mac_add):
    conn = get_db_connection()
    cursor = conn.cursor()

    #sql = "SELECT * FROM sample WHERE capture_tim=%i" % (box_id,)
    #sql = 'SELECT * FROM test'
    sql = "SELECT DISTINCT test_id from power_event_system_%s WHERE mac_address = '%s' ORDER BY test_id ASC" %(table_sufix, mac_add)
    print(sql)
    nrows = cursor.execute(sql)
    data = dictfetchall(cursor)
    
    cursor.close()
    conn.close()
    #response = Response(data, mimetype='text/json')
    #response.headers['Content-Disposition'] = 'attachment; filename=sample_%i.csv' % (box_id,)
    response = Response(json.dumps(data), mimetype='application/json')
    return response

#POWER EVENTS GET POST
# @app.route('/api/box/<int:box_id>/power_event', methods=['POST'])
# def power_event(box_id):
#     try:
#         jsonObj = json.loads(request.data)     # will thow error if not json (if the key was wrong) not prob, flask will reload itself
#         if jsonObj:
#             conn = get_db_connection()
#             query = conn.cursor()
#             sql = "INSERT INTO sample (box_id, impulse_prod, kw_prod, impulse_cons, kw_cons, voltage, capture_timestamp) VALUES %(values)s;"
#             values = ",".join(["(%i,%i,%f,%i,%f,%f,'%s')" % (box_id,
#                                                     int(row['impulse_prod']),
#                                                     float(row['kw_prod']),
#                                                     int(row['impulse_cons']),
#                                                     float(row['kw_cons']),
#                                                     float(row['voltage']),
#                                                     conn.escape_string(row['capture_timestamp']),) for row in jsonObj])
#             nrows = query.execute(sql % {'values': values})
#             conn.commit()
#             query.close()
#             conn.close()
#             #redirectCopy(jsonObj, "http://li1097-240.members.linode.com:8100/api/box/", box_id, "sample")
#             return jsonify({'rows':nrows})
#         return make_response("error: bad json", 400)
#     except: return make_response("error: wrong encryption key", 400)

#
#GET AND POST for new boxes samples. inserts in the sample_mt sample
#
# @app.route('/api/box/<int:box_id>/sample_mt', methods=['POST'])
# def post_sample_mt(box_id):
	
# 	print "------"
# 	#print request
# 	#print request.values
# 	encripted_data = request.get_data() #request.form.keys()[0].replace(" ","+")+"=="
# 	print encripted_data
# 	print "------"
# 	decrypted = CryptoECB.decryptECB(encripted_data, ENCRYPTION_KEY)
# 	decrypted = decrypted.split(']')[0]+']'
	
# 	print repr(decrypted)
	
# 	jsonObj = json.loads(decrypted)		# will thow error if not json (if the key was wrong) not prob, flask will reload itself
	
# 	if jsonObj:
# 		conn = get_db_connection()
# 		query = conn.cursor()
# 		sql = "Insert into sample_mt(box_id,pmax,act_energy_T1,act_energy_T2,act_energy_T3,act_energy_T4,react_energy_T1,react_energy_T2,capture_timestamp) VALUES %(values)s;"
# 		values = ",".join(["(%i,%f,%f,%f,%f,%f,%f,%f,'%s')" % (int(row['identifier']),
# 												float(row['pmax']),
# 												float(row['act_energy_T1']),
# 												float(row['act_energy_T2']),
# 												float(row['act_energy_T3']),
# 												float(row['act_energy_T4']),
# 												float(row['react_energy_T1']),
# 												float(row['react_energy_T2']),
# 												conn.escape_string(row['capture_timestamp']),) for row in jsonObj])
# 		nrows = query.execute(sql % {'values': values})
# 		conn.commit()
# 		query.close()
# 		conn.close()
# 		#redirectCopy(jsonObj, "http://li1097-240.members.linode.com:8100/api/box/", box_id, "sample")
# 		return jsonify({'rows':nrows})
# 	return make_response("error: bad json", 400)
# 	#except: return make_response("error: wrong encryption key", 400)


# @app.route('/api/box/<int:box_id>/sample_mt', methods=['GET'])
# @app.route('/api/box/<int:box_id>/sample_mt/<int:limit>', methods=['GET'])
# @app.route('/api/box/<int:box_id>/sample_mt/<int:limit>/<path:dateStart>', methods=['GET'])
# @app.route('/api/box/<int:box_id>/sample_mt/<int:limit>/<path:dateStart>/<path:dateEnd>', methods=['GET'])
# def get_sample_mt(box_id, limit=0, dateStart="none", dateEnd="none"):
# 	conn = get_db_connection()
# 	query = conn.cursor()

# 	sql = "SELECT * FROM sample_mt WHERE box_id=%i" % (box_id,)
# 	if(dateStart != "none"):
# 		sql += " AND capture_timestamp >= '" + conn.escape_string(dateStart) + "'"
# 	if(dateEnd != "none"):
# 		dateEnd += " 23:59:59"
# 		sql += " AND capture_timestamp <= '" + conn.escape_string(dateEnd) + "'"
# 	if(limit != 0):
# 		sql += " Limit " + str(limit)

# 	nrows = query.execute(sql)
# 	rows = query.fetchall()
# 	data = "id,box_id,pmax,act_energy_T1,act_energy_T2,act_energy_T3,act_energy_T4,react_energy_T1,react_energy_T2,capture_timestamp\n" + "\n".join([','.join(map(str, row)) for row in rows])
# 	query.close()
# 	conn.close()
# 	response = Response(data, mimetype='text/csv')
# 	response.headers['Content-Disposition'] = 'attachment; filename=sample_%i.csv' % (box_id,)
# 	return response

#
#	
#

	
#@app.route('/static')
#def send_js():
#    return send_file('static/index.html')

@app.route('/', methods=['GET'])
def hello():
    redirect = """
        HybridNILM for smart semi-labeling
       """
    return redirect

    # NONE OF THIS GREAT CODE WORKS!!! WHYYYY ?!?!? fuck this shit
    #return redirect("http://ec2-52-17-209-120.eu-west-1.compute.amazonaws.com/static/index.html", code=302)
    #return send_js('index.html')
    #return render_template('static/index.html')
    #return redirect('http://ec2-52-17-209-120.eu-west-1.compute.amazonaws.com/static/index.html')
    #return send_file('static/index.html', mimetype='text/html')
    #return send_from_directory('static', "index.html")

@app.route('/static/<path:path>', methods=['GET'])
def send_static(path):
    return send_from_directory('static', path)

if __name__ == '__main__':
    #ensure_db_tables()
    app.run(debug = True, port = 8000, host = '0.0.0.0',threaded=True)

    # TORNADO START
    # http_server = HTTPServer(WSGIContainer(app))
    # http_server.listen(8000)
    # IOLoop.instance().start()

    # GEVENT START
    #http_server = WSGIServer(('', 8000), app)
    #http_server.serve_forever()
