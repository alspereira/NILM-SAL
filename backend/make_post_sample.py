import json
import urllib2							#2.7
import requests
from os import system
import time
import datetime

#import mycrypt as Crypto
#import mycryptECB as CryptoECB

# def uploadData(valueList, jsonKeys, BOX_ID, BASE_URL, table, encryption_key=""):
# 	"""
# 	Uploads value list to web server by POST. The values should be given in the correct order
# 	:param values: list of values in the order :produced, consumed, timestamp, voltage, consumed2
# 	:return: Returns True or False accordingly if the upload was successful
# 	"""
# 	jsonValues = {}
# 	for i in range(len(valueList)):
# 		jsonValues[jsonKeys[i]] = valueList[i]
# 	jsonValues = [jsonValues]	#needs to be array

# 	jsonStr = json.dumps(jsonValues)
	
# 	print jsonStr
	
# 	if(encryption_key != ""):
# 		jsonStr = Crypto.encrypt(jsonStr, encryption_key)

# 	print jsonStr
		
# 	if not BASE_URL.endswith("/"):
# 		BASE_URL += '/'
# 	composedUrl = BASE_URL + str(BOX_ID) + '/' + table
# 	print composedUrl
# 	try:
# 		headers = {'Content-type': 'application/json'}
# 		r = requests.post(composedUrl, data=jsonStr, headers=headers)
# 		if r.status_code == requests.codes.ok:
# 			print ('SENT SENT SENT SENT SENT SENT SENT SENT SENT')
# 			return True
# 		else:
# 			print ('NOT NOT NOT NOT SENT NOT NOT NOT  NOT SENT')
# 			print (jsonValues)
# 			print (r)
# 			print (r.raw)
# 			return False
# 	except:
# 		return False
		
# def uploadDataV2(valueList, jsonKeys, BOX_ID, BASE_URL, table, encryption_key=""):
# 	"""
# 	Uploads value list to web server by POST. The values should be given in the correct order
# 	:param values: list of values in the order :produced, consumed, timestamp, voltage, consumed2
# 	:return: Returns True or False accordingly if the upload was successful
# 	"""
# 	jsonValues = {}
# 	for i in range(len(valueList)):
# 		jsonValues[jsonKeys[i]] = valueList[i]
# 	jsonValues = [jsonValues]	#needs to be array

# 	jsonStr = json.dumps(jsonValues)
# 	if(encryption_key != ""):
# 		jsonStr = Crypto.encrypt2(jsonStr, encryption_key)

# 	print ('Encripted message')
# 	print ('--------')
# 	print (jsonStr)
# 	print ('--------')
	
# 	if not BASE_URL.endswith("/"):
# 		BASE_URL += '/'
# 	composedUrl = BASE_URL + str(BOX_ID) + '/' + table
# 	try:
# 		headers = {'Content-type': 'application/json'}
# 		r = requests.post(composedUrl, data=jsonStr, headers=headers)
# 		if r.status_code == requests.codes.ok:
# 			print ('SENT SENT SENT SENT SENT SENT SENT SENT SENT')
# 			return True
# 		else:
# 			print ('NOT NOT NOT NOT SENT NOT NOT NOT  NOT SENT')
# 			print (jsonValues)
# 			print (r)
# 			print (r.raw)
# 			return False
# 	except:
# 		return False
		
# def uploadDataSimple(valueList, jsonKeys, BOX_ID, BASE_URL, table, encryption_key=""):

# 	jsonValues = {}
# 	for i in range(len(valueList)):
# 		jsonValues[jsonKeys[i]] = valueList[i]
# 	jsonValues = [jsonValues]	#needs to be array

# 	jsonStr = json.dumps(jsonValues)
# 	if(encryption_key != ""):
# 		jsonStr = CryptoECB.encryptECB(jsonStr, encryption_key)

# 	print ('Encripted message')
# 	print ('--------')
# 	print (jsonStr)
# 	print ('--------')
	
# 	if not BASE_URL.endswith("/"):
# 		BASE_URL += '/'
# 	composedUrl = BASE_URL + str(BOX_ID) + '/' + table
# 	print composedUrl
# 	try:
# 		headers = {'Content-type': 'application/json'}
# 		r = requests.post(composedUrl, data=jsonStr, headers=headers)
# 		print r
# 		if r.status_code == requests.codes.ok:
# 			print ('SENT SENT SENT SENT SENT SENT SENT SENT SENT')
# 			return True
# 		else:
# 			print ('NOT NOT NOT NOT SENT NOT NOT NOT  NOT SENT')
# 			print (jsonValues)
# 			print (r)
# 			print (r.raw)
# 			return False
# 	except:
# 		return False


# def uploadDataString(dataString, BASE_URL, table, encryption_key=""):

# 	'''
# 	jsonValues = {}
# 	for i in range(len(valueList)):
# 		jsonValues[jsonKeys[i]] = valueList[i]
# 	jsonValues = [jsonValues]	#needs to be array

# 	jsonStr = json.dumps(jsonValues)
# 	'''
# 	BOX_ID = 10999911
# 	if(encryption_key != ""):
# 		jsonStr = CryptoECB.encryptECB(dataString, encryption_key)

# 	print ('Encripted message')
# 	print ('--------')
# 	print (jsonStr)
# 	print ('--------')
	
# 	if not BASE_URL.endswith("/"):
# 		BASE_URL += '/'
# 	composedUrl = BASE_URL + str(BOX_ID) + '/' + table
# 	print composedUrl
# 	try:
# 		headers = {'Content-type': 'application/json'}
# 		r = requests.post(composedUrl, data=jsonStr, headers=headers)
# 		print r
# 		if r.status_code == requests.codes.ok:
# 			print ('SENT SENT SENT SENT SENT SENT SENT SENT SENT')
# 			return True
# 		else:
# 			print ('NOT NOT NOT NOT SENT NOT NOT NOT  NOT SENT')
# 			print (jsonValues)
# 			print (r)
# 			print (r.raw)
# 			return False
# 	except:
# 		return False
		
def uploadDataPWPEH(url, obj):
	jsonStr = json.dumps(obj)
	try:
		#headers = {'Content-type': 'application/json'}
		print headers
		print url
		r = requests.post(url, data=jsonStr, headers=headers)
		if r.status_code == requests.codes.ok:
			print ('SENT')
			return True
		else:
			print ('NOT SENT')			
			print (r.text)
			#print (r.raw)
			return False
	except:
		return False

if __name__ == "__main__":
	
	url = "http://aveiro.m-iti.org:8000/api/power-event/add/mac_add/000D6F000099265F"
	url = "http://localhost:8000/api/power-event/add/mac_add/000D6F000099265F"
	#url = "http://127.0.0.1/api/box/"
	SEND_keys = ['impulse_prod', 'kw_prod', 'impulse_cons', 'kw_cons', 'voltage', 'wflow_ticks','lpm','capture_timestamp']
	timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
	valueList = [4,1.9443,4,1.4353,11,23.43,229.4,timestamp]
	encryptionKey = "8Gc*8-eJ82Wkp|E4_2+qQPV^XXB5N29v"
	#uploadData(valueList, SEND_keys, 999911, url, "sample", encryptionKey)

	obj = {
		"test_id": 1,
		"action": 'add',
		#"mac_address": '000D6F000099265F',
		"timestamp": '2016-12-01',
		"initial_power": 160,
		"delta" :120
	}
	print "chegou aki"
	uploadDataPWPEH(url, obj)
	
	"""
	"""
	"""
	"identifier": 84513109,
"pmax": 0005,
"act_energy_T1": 00227185,
"act_energy_T2": 00077682,
"act_energy_T3": 00130759,
"act_energy_T4": 00014849,
"Tact_energy1": 00435626,
"react_energy_T1": 00014849,
"react_energy_T2": 00010242,
"capture_timestamp": "2016-02-16 13:00:00"

AES_ECB+BASE64

[{"identifier":"84513109","pmax":"0000","act_energy_T1":"00227185","act_energy_T2":"00077682","act_energy_T3":"00130759","act_energy_T4":"00000000","react_energy_T1":"00014849","react_energy_T2":"00010242","capture_timestamp":"2016-03-07 17:33:00"}]

8Gc*8-eJ82Wkp|E4_2+qQPV^XXB5N29v

http://aesencryption.net/

	
	
	url = "http://ec2-52-17-209-120.eu-west-1.compute.amazonaws.com/api/box/"
	#url = "http://172.31.21.105:3333/api/box/"
	#url = "http://127.0.0.1/api/box/"
	SEND_keys = ['identifier', 'pmax', 'act_energy_T1', 'act_energy_T2', 'act_energy_T3', 'act_energy_T4','Tact_energy1','react_energy_T1','react_energy_T2','capture_timestamp']
	timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y/%m/%d %H:%M:%S')
	valueList = [84513109,5,227185,77682,130759,14849,435626,14849,10242,timestamp]
	#valueList = [84513109,0005,00227185,00077682,00130759,00014849,00435626,00014849,00010242,timestamp]
	encryptionKey = "8Gc*8-eJ82Wkp|E4_2+qQPV^XXB5N29v"
	uploadData(valueList, SEND_keys, 10999911, url, "sample", encryptionKey)
	#jsstring = '[{"identifier":"84513109","pmax":"0000","act_energy_T1":"00227185","act_energy_T2":"00077682","act_energy_T3":"00130759","act_energy_T4":"00000010","react_energy_T1":"00014849","react_energy_T2":"00010242","capture_timestamp":"2016-03-07 17:33:00"}]'
	#uploadDataString(jsstring,	url, "sample_mt", encryptionKey)
	
#	jsstring = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
	
#	message	 = Crypto.encrypt2(jsstring,encryptionKey)
#	print (message)
#	message2 = Crypto.decrypt2(message,encryptionKey)
	#print (message2)
	#print(Crypto.decryptECB("411AIqJgi/PCA6S53UECwcVnAhMMbfOSyI/kFjuqWN9TQSGnbwnQzPXUOHMvldKZ/D0//q5pCAYflK7k8on3g8tAnSAzV6/otXHb3fl3hYx7tj8BeVcZeHAqFpPfqSzLW2ibcqR4sNKZZLHqAnS3gWNkeheiJmnjC2ZbK6QiKEgxASANTXLmQFRU4YVGiOnUy6kMxy97hgGe0ATCaPXxisXryWonZfvLb+4NBEL5ZwX7lBATqYG6y4g0QbdVVir1Jd6BCs+JiZ2sLBnfmUtsC2owcpv+lyeRl7K87ZGoiqd+iDcBApCWgNe82RMwUqs3yFvGrsJsoOhx7feaXjk/hw==",encryptionKey))
	#print Crypto.encryptECB("merda merda coisas assim",encryptionKey)
	#print ('working here dawg')
	
	url = "http://ec2-52-17-209-120.eu-west-1.compute.amazonaws.com/api/box/"
	#url = "http://127.0.0.1/api/box/"
	SEND_keys = ['impulse_prod', 'kw_prod', 'impulse_cons', 'kw_cons', 'voltage', 'capture_timestamp']
	timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y/%m/%d %H:%M:%S')
	valueList = [4,1.9443,4,1.4353,229.4,timestamp]
	encryptionKey = "8Gc*8-eJ82Wkp|E4_2+qQPV^XXB5N29v"
	SEND_keys2 = ['identifier', 'pmax', 'act_energy_T1', 'act_energy_T2', 'act_energy_T3', 'act_energy_T4','Tact_energy1','react_energy_T1','react_energy_T2','capture_timestamp']
	#uploadData(valueList, SEND_keys, 999911, url, "sample", encryptionKey)
	valueList2 = [84513109,5,227185,77682,130759,14849,435626,14849,10242,timestamp]
	uploadDataSimple(valueList2, SEND_keys2, 999911, url, "sample_mt", encryptionKey)"""