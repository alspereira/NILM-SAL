Semi-Automatic Labeling of NILM Datasets

This is the source-code used in the following publication:

L. Pereira, M. Ribeiro, and N. Nunes, “Engineering and deploying a hardware and software platform to collect and label non-intrusive load monitoring datasets,” in 2017 Sustainable Internet and ICT for Sustainability (SustainIT), Funchal, Portugal, 2017, pp. 1–9.
